var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body

var app = express()  // make express app
var http = require('http').Server(app)  // inject app into the server

// ADD THESE COMMENTS AND IMPLEMENTATION HERE 

app.use(express.static(__dirname + '/assets'))

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine

// 2 create an array to manage our entries app.locals is a built in object
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  response.render("page1")
})
app.get("/page1", function (request, response) {
  response.render("page1")
})
app.get("/mychoicepage", function (request, response) {
    response.render("mychoicepage")
  })
  app.get("/contactpage", function (request, response) {
    response.render("contactpage")
  })
  app.get("/guestbook", function (request, response) {
    response.render("guestbook")
  })
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})

// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
console.log(request.body);

  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/guestbook")  // where to go next? Let's go to the home page :)
})


app.post("/contact",function(request,response) {
  var api_key = 'key-d11bf87d75c75adbd6edd7932ca66c6b';
  var domain = 'sandbox329b9af08762411fbca6f89c8635c17d.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'New Mail from customer <postmaster@sandbox329b9af08762411fbca6f89c8635c17d.mailgun.org>',
    to: 's531367@nwmissouri.edu',
    subject: request.body.name,
    text:request.body.email+'\n'+request.body.feedback
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error)
    response.send("Mail Sent");
    else
    response.send("Mail not Sent");
  });




})












// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})

// Listen for an application request on port 8081 & notify the developer
//http.listen(8081, function () {
 // console.log('Guestbook app listening on http://127.0.0.1:8081/')
//})


// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
})
